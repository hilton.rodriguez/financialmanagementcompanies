package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntBusinessManager {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    @Size(max = 8, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String financialManagementCompanyId;
    @Size(max = 30, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String fullName;
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String identityDocumentDocumentTypeId;
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    @Size(max = 12, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String identityDocumentDocumentNumber;

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContactType0;

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    @Size(max = 78, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContact0;

    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContactType1;
    @Size(max = 78, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContact1;
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContactType2;
    @Size(max = 78, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContact2;
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContactType3;
    @Size(max = 78, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String contactDetailsContact3;
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String rolesId0;
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String rolesId1;
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesBusinessManager.class)
    private String rolesId2;

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentityDocumentDocumentTypeId() {
        return identityDocumentDocumentTypeId;
    }

    public void setIdentityDocumentDocumentTypeId(String identityDocumentDocumentTypeId) {
        this.identityDocumentDocumentTypeId = identityDocumentDocumentTypeId;
    }

    public String getIdentityDocumentDocumentNumber() {
        return identityDocumentDocumentNumber;
    }

    public void setIdentityDocumentDocumentNumber(String identityDocumentDocumentNumber) {
        this.identityDocumentDocumentNumber = identityDocumentDocumentNumber;
    }

    public String getContactDetailsContactType0() {
        return contactDetailsContactType0;
    }

    public void setContactDetailsContactType0(String contactDetailsContactType0) {
        this.contactDetailsContactType0 = contactDetailsContactType0;
    }

    public String getContactDetailsContact0() {
        return contactDetailsContact0;
    }

    public void setContactDetailsContact0(String contactDetailsContact0) {
        this.contactDetailsContact0 = contactDetailsContact0;
    }

    public String getContactDetailsContactType1() {
        return contactDetailsContactType1;
    }

    public void setContactDetailsContactType1(String contactDetailsContactType1) {
        this.contactDetailsContactType1 = contactDetailsContactType1;
    }

    public String getContactDetailsContact1() {
        return contactDetailsContact1;
    }

    public void setContactDetailsContact1(String contactDetailsContact1) {
        this.contactDetailsContact1 = contactDetailsContact1;
    }

    public String getContactDetailsContactType2() {
        return contactDetailsContactType2;
    }

    public void setContactDetailsContactType2(String contactDetailsContactType2) {
        this.contactDetailsContactType2 = contactDetailsContactType2;
    }

    public String getContactDetailsContact2() {
        return contactDetailsContact2;
    }

    public void setContactDetailsContact2(String contactDetailsContact2) {
        this.contactDetailsContact2 = contactDetailsContact2;
    }

    public String getContactDetailsContactType3() {
        return contactDetailsContactType3;
    }

    public void setContactDetailsContactType3(String contactDetailsContactType3) {
        this.contactDetailsContactType3 = contactDetailsContactType3;
    }

    public String getContactDetailsContact3() {
        return contactDetailsContact3;
    }

    public void setContactDetailsContact3(String contactDetailsContact3) {
        this.contactDetailsContact3 = contactDetailsContact3;
    }

    public String getRolesId0() {
        return rolesId0;
    }

    public void setRolesId0(String rolesId0) {
        this.rolesId0 = rolesId0;
    }

    public String getRolesId1() {
        return rolesId1;
    }

    public void setRolesId1(String rolesId1) {
        this.rolesId1 = rolesId1;
    }

    public String getRolesId2() {
        return rolesId2;
    }

    public void setRolesId2(String rolesId2) {
        this.rolesId2 = rolesId2;
    }
}