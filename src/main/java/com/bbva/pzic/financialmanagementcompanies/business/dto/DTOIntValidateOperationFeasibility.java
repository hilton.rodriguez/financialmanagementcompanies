package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntValidateOperationFeasibility {

    @Valid
    @NotNull(groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private DTOIntContractValidate contract;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
    private DTOIntAmount operationAmount;
    private DTOIntResult result;

    public DTOIntContractValidate getContract() {
        return contract;
    }

    public void setContract(DTOIntContractValidate contract) {
        this.contract = contract;
    }

    public DTOIntAmount getOperationAmount() {
        return operationAmount;
    }

    public void setOperationAmount(DTOIntAmount operationAmount) {
        this.operationAmount = operationAmount;
    }

    public DTOIntResult getResult() {
        return result;
    }

    public void setResult(DTOIntResult result) {
        this.result = result;
    }
}
