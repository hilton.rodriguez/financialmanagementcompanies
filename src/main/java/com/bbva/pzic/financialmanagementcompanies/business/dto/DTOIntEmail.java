package com.bbva.pzic.financialmanagementcompanies.business.dto;

public class DTOIntEmail {

    private DTOIntType type;

    private String value;

    public DTOIntType getType() {
        return type;
    }

    public void setType(DTOIntType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
