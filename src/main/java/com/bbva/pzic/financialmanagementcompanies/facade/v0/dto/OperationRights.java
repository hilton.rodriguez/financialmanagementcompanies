package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "operationRights", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "operationRights", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class OperationRights implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Type of attributes
     */
    private Indicators indicators;
    /**
     * Type of permission.
     */
    private PermissionType permissionType;
    /**
     * Signature information. It only applies if permissionType is SIGNATURE or
     * QUERY_AND_SIGNATURE.
     */
    private Signature signature;

    public Indicators getIndicators() {
        return indicators;
    }

    public void setIndicators(Indicators indicators) {
        this.indicators = indicators;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(PermissionType permissionType) {
        this.permissionType = permissionType;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }
}
