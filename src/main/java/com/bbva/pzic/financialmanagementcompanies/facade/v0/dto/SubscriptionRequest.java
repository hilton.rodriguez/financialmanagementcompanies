package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "subscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "subscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubscriptionRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business information that will be registered in the subscription request.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private BusinessSubscriptionRequest business;
    /**
     * Limit amount for the chosen netcash product.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private LimitAmount limitAmount;
    /**
     * Contracts related to the subscription request.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private List<RelatedContract> relatedContracts;
    /**
     * Financial product associated to the solicitude.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private ProductSubscriptionRequest product;
    /**
     * Types of joint business.
     */
    private Joint joint;
    /**
     * Business manager information. A business manager is a natural person that
     * belongs to the company.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private List<BusinessManagerSubscriptionRequest> businessManagers;
    /**
     * Management unit identifier of the branch. A unit management is conformed
     * by a group of executives who attends a specific operations in a branch. A
     * branch can have several units management.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    private String unitManagement;
    /**
     * Office in which the account was created.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private Branch branch;
    /**
     * Subscription request identifier.
     */
    private String id;
    /**
     * Status information.
     */
    private StatusSubscriptionRequest status;
    /**
     * Solicitude opening date. ISO-8601 date format (AAAA-MM-DD).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date openingDate;
    /**
     * Reviewer information.
     */
    private List<Reviewer> reviewers;
    /**
     * Subscription request comment. The comment is written by the participant
     * who manages the request.
     */
    private String comment;

    public BusinessSubscriptionRequest getBusiness() {
        return business;
    }

    public void setBusiness(BusinessSubscriptionRequest business) {
        this.business = business;
    }

    public LimitAmount getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(LimitAmount limitAmount) {
        this.limitAmount = limitAmount;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public ProductSubscriptionRequest getProduct() {
        return product;
    }

    public void setProduct(ProductSubscriptionRequest product) {
        this.product = product;
    }

    public Joint getJoint() {
        return joint;
    }

    public void setJoint(Joint joint) {
        this.joint = joint;
    }

    public List<BusinessManagerSubscriptionRequest> getBusinessManagers() {
        return businessManagers;
    }

    public void setBusinessManagers(
            List<BusinessManagerSubscriptionRequest> businessManagers) {
        this.businessManagers = businessManagers;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatusSubscriptionRequest getStatus() {
        return status;
    }

    public void setStatus(StatusSubscriptionRequest status) {
        this.status = status;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public List<Reviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<Reviewer> reviewers) {
        this.reviewers = reviewers;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
