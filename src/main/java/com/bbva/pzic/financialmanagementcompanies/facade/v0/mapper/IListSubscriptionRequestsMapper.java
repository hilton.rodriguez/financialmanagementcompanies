package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequests;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IListSubscriptionRequestsMapper {

    InputListSubscriptionRequests mapIn(String subscriptionsRequestId,
                                        String businessDocumentsBusinessDocumentTypeId,
                                        String businessDocumentsDocumentNumber, String statusId,
                                        String unitManagement, String branchId,
                                        String fromSubscriptionRequestDate,
                                        String toSubscriptionRequestDate, String businessId,
                                        String paginationKey, Integer pageSize);

    SubscriptionRequests mapOut(
            DTOIntSubscriptionRequests dtoIntSubscriptionRequests);
}
