package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "relatedProduct", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "relatedProduct", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RelatedProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Product code.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    private String id;
    /**
     * Financial product associated to the contract.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private ProductType productType;
    /**
     * Product name.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
