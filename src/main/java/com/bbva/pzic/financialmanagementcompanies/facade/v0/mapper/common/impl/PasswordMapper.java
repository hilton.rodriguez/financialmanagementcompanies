package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.common.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestHeader;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.common.IPasswordMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 24/09/2018.
 *
 * @author Entelgy
 */
@Component
public class PasswordMapper implements IPasswordMapper {

    public static final String LDAP_DEL_USER = "smc.configuration.SMCPE1810337.ldapNetcash.delUser";
    public static final String LDAP_DEL_USER_FORM_GROUP = "smc.configuration.SMCPE1810337.ldapNetcash.delUserFromGroup";

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public DTOIntRequestHeader mapHeader() {
        DTOIntRequestHeader requestHeader = new DTOIntRequestHeader();
        requestHeader.setAap(serviceInvocationContext.getProperty(BackendContext.AAP));
        requestHeader.setUser(serviceInvocationContext.getUser());
        requestHeader.setServiceID("SMCPE1810337");
        requestHeader.setRequestID(serviceInvocationContext.getInternalRquestId());
        requestHeader.setContactID(serviceInvocationContext.getProperty(BackendContext.CONTACT_ID));
        return requestHeader;
    }

    @Override
    public DTOIntRequestBody mapBody(String aap) {
        DTOIntRequestBody requestBody = new DTOIntRequestBody();
        requestBody.setOperation(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.newUserSimplePwdChg"));
        requestBody.setCountry(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.country"));
        requestBody.setDataOperationBank(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.dataOperation.bank"));
        requestBody.setDataOperationPdgroup(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.dataOperation.pdgroup"));
        requestBody.setDataOptionalsLdap(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.dataOptionals.ldap"));
        requestBody.setDataOptionalsRama(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.dataOptionals.rama"));
        requestBody.setDataOptionalsGrupo(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.dataOptionals.grupo"));
        requestBody.setDataOptionalsPrefijo(mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.dataOptionals.prefijo"));
        return requestBody;
    }

    @Override
    public String mapLdapField(final String aap, final String key) {
        return enumMapper.getPropertyValueApp(aap, key);
    }
}
