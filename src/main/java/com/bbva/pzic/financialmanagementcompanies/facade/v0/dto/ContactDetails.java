package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "contactDetails", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "contactDetails", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information identifier.
     */
    private String id;
    /**
     * Contact information value.
     */
    private String contact;
    /**
     * Contact information type identifier. DISCLAIMER: A business manager can only have one LOGIN_EMAIL.
     * (annotations.countrySpecific):
     * - country: PE
     * description: MOBILE_NUMBER, PHONE_NUMBER and WORK_EMAIL are implemented in Peru.
     */
    private String contactType;
    /**
     * Contact country code.
     */
    private String regionalCode;
    /**
     * Contact extension.
     */
    private String extension;
    /**
     * Indicates whether this contact information is the preferred one related by the customer for the current contact information type.
     */
    private Boolean isPreferential;
    /**
     * Indicates whether the contact information has been verified, this contact must be use by default to send notifications to the client; one related by the customer per contact information type.
     * DISCLAIMER: Only LOGIN_EMAIL and MOBILE_NUMBER can be checked.
     */
    private Boolean isChecked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getRegionalCode() {
        return regionalCode;
    }

    public void setRegionalCode(String regionalCode) {
        this.regionalCode = regionalCode;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Boolean getPreferential() {
        return isPreferential;
    }

    public void setPreferential(Boolean preferential) {
        isPreferential = preferential;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
