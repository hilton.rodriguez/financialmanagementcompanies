package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper extends ConfigurableMapper implements ICreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper {

    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(CreateAuthorizedBusinessManager.class, InputCreateAuthorizedBusinessManager.class)
                .field("businessManager.id", "businessManagerId")
                .field("businessManagement.operationsRights.signature.validationRights.id", "validationRightsId")
                .field("businessManagement.operationsRights.permissionType.id", "permissionTypeId")
                .field("businessManagement.status", "businessManagementStatus")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputCreateAuthorizedBusinessManager mapIn(final String financialManagementCompanyId, final CreateAuthorizedBusinessManager createAuthorizedBusinessManager) {
        LOG.info("... called method CreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper.mapIn ...");
        InputCreateAuthorizedBusinessManager input = map(createAuthorizedBusinessManager, InputCreateAuthorizedBusinessManager.class);
        input.setFinancialManagementCompanyId(financialManagementCompanyId);
        input.setValidationRightsId(enumMapper.getBackendValue("authorizedBusinessManager.signature.id", input.getValidationRightsId()));
        input.setPermissionTypeId(enumMapper.getBackendValue("authorizedBusinessManager.operationsRights.permissionType.id", input.getPermissionTypeId()));
        input.setBusinessManagementStatus(enumMapper.getBackendValue("authorizedBusinessManager.status", input.getBusinessManagementStatus()));
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<CreateAuthorizedBusinessManager> mapOut(final CreateAuthorizedBusinessManager data) {
        LOG.info("... called method CreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper.mapOut ...");
        if (data == null) {
            return null;
        }
        return ServiceResponse.data(data).pagination(null).build();
    }
}
