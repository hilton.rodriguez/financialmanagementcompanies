package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessManager", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessManager", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessManager implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Customer identifier for channel control purposes: channel + internal bank + reference + user".
     * (annotations.countryException):
     * - country: PE
     * description: In Peru, this attribute is optional.
     */
    private String targetUserId;
    /**
     * Financial management company identifier.
     */
    private String financialManagementCompanyId;
    /**
     * Contact information identifier.
     */
    private String id;
    /**
     * Manager\'s full name. It contains name and surname.
     * DISCLAIMER: It will be mandatory when firstName attribute is not fulfilled.
     */
    private String fullName;
    /**
     * Manager\'s first name. DISCLAIMER: It will be mandatory when fullName attribute is not fulfilled.
     */
    private String firstName;
    /**
     * Manager\'s middle name.
     */
    private String middleName;
    /**
     * Manager\'s last name.
     */
    private String lastName;
    /**
     * Manager\'s second last name.
     */
    private String secondLastName;
    /**
     * Indicator that tells wether the manager is blocked or not.
     */
    private Boolean isBlocked;
    /**
     * Manager registration date.
     * (annotations.countryException):
     * - country: PE
     * description: In Peru, this attribute is optional.
     */
    private Date creationDate;
    /**
     * Manager\'s identity document.
     */
    private IdentityDocument identityDocument;
    /**
     * Manager\'s contact details.
     */
    private List<ContactDetail> contactDetails;
    /**
     * Business manager role information.
     */
    private List<Role> roles;

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
