package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import java.util.List;

public class ModelFinancialManagementCompanies2 {

    private ModelBusiness business;
    private ModelNetcashType netcashType;
    private ModelContract contract;
    private ModelProduct product;
    private ModelRelationType relationType;
    private List<ModelReviewer> reviewers;

    public ModelBusiness getBusiness() {
        return business;
    }

    public void setBusiness(ModelBusiness business) {
        this.business = business;
    }

    public ModelNetcashType getNetcashType() {
        return netcashType;
    }

    public void setNetcashType(ModelNetcashType netcashType) {
        this.netcashType = netcashType;
    }

    public ModelContract getContract() {
        return contract;
    }

    public void setContract(ModelContract contract) {
        this.contract = contract;
    }

    public ModelProduct getProduct() {
        return product;
    }

    public void setProduct(ModelProduct product) {
        this.product = product;
    }

    public ModelRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(ModelRelationType relationType) {
        this.relationType = relationType;
    }

    public List<ModelReviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<ModelReviewer> reviewers) {
        this.reviewers = reviewers;
    }

}
