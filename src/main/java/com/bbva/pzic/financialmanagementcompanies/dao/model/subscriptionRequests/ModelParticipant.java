package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelParticipant {

    private String id;
    private String code;
    @DatoAuditable(omitir = true)
    private String firstName;
    @DatoAuditable(omitir = true)
    private String middleName;
    @DatoAuditable(omitir = true)
    private String lastName;
    @DatoAuditable(omitir = true)
    private String motherLastName;
    private List<ModelContactInformation> contactInformations;
    private ModelParticipantType participantType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMotherLastName() {
        return motherLastName;
    }

    public void setMotherLastName(String motherLastName) {
        this.motherLastName = motherLastName;
    }

    public List<ModelContactInformation> getContactInformations() {
        return contactInformations;
    }

    public void setContactInformations(List<ModelContactInformation> contactInformations) {
        this.contactInformations = contactInformations;
    }

    public ModelParticipantType getParticipantType() {
        return participantType;
    }

    public void setParticipantType(ModelParticipantType participantType) {
        this.participantType = participantType;
    }
}