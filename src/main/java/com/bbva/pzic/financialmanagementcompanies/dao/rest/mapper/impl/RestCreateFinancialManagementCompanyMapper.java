package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Mapper
public class RestCreateFinancialManagementCompanyMapper implements IRestCreateFinancialManagementCompanyMapper {
    @Override
    public ModelFinancialManagementCompanies2 mapIn(final DTOIntFinancialManagementCompanies dtoIntFinancialManagementCompanies) {
        if(dtoIntFinancialManagementCompanies == null){
            return null;
        }
        ModelFinancialManagementCompanies2 model = new ModelFinancialManagementCompanies2();
        model.setBusiness(mapInBusiness(dtoIntFinancialManagementCompanies.getBusiness()));
        model.setNetcashType(mapInNetcashType(dtoIntFinancialManagementCompanies.getNetcashType()));
        model.setContract(mapInContract(dtoIntFinancialManagementCompanies.getContract()));
        model.setProduct(mapInProduct(dtoIntFinancialManagementCompanies.getProduct()));
        model.setRelationType(mapInRelationType(dtoIntFinancialManagementCompanies.getRelationType()));
        model.setReviewers(mapInReviewers(dtoIntFinancialManagementCompanies.getReviewers()));

        return model;
    }

    private List<ModelReviewer> mapInReviewers(final List<DTOIntReviewerNetcash> reviewers) {
        if(CollectionUtils.isEmpty(reviewers)){
            return null;
        }
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private ModelReviewer mapInReviewer(final DTOIntReviewerNetcash dtoIntReviewerNetcash) {
        ModelReviewer model = new ModelReviewer();
        model.setBusinessAgentId(dtoIntReviewerNetcash.getBusinessAgentId());
        model.setContactDetails(mapInContactDetails(dtoIntReviewerNetcash.getContactDetails()));
        model.setReviewerType(mapInReviewerType(dtoIntReviewerNetcash.getReviewerType()));
        model.setUnitManagement(dtoIntReviewerNetcash.getUnitManagement());
        model.setBank(mapInBank(dtoIntReviewerNetcash.getBank()));
        model.setProfile(mapInProfile(dtoIntReviewerNetcash.getProfile()));
        model.setRegistrationIdentifier(dtoIntReviewerNetcash.getRegistrationIdentifier());
        return model;
    }

    private ModelProfile mapInProfile(final DTOIntProfile profile) {
        if(profile == null){
            return null;
        }
        ModelProfile model = new ModelProfile();
        model.setId(profile.getId());
        return model;
    }

    private ModelBank mapInBank(final DTOIntBank bank) {
        if(bank == null){
            return null;
        }
        ModelBank model = new ModelBank();
        model.setId(bank.getId());
        model.setBranch(mapInBranch(bank.getBranch()));
        return model;
    }

    private ModelBranch mapInBranch(final DTOIntBranch branch) {
        if(branch == null){
            return null;
        }
        ModelBranch model = new ModelBranch();
        model.setId(branch.getId());
        return model;
    }

    private ModelReviewerType mapInReviewerType(final DTOIntReviewerType reviewerType) {
        if(reviewerType == null){
            return null;
        }
        ModelReviewerType model = new ModelReviewerType();
        model.setId(reviewerType.getId());
        return model;
    }

    private List<ModelContactDetail> mapInContactDetails(final List<DTOIntContactDetail> contactDetails) {
        if(CollectionUtils.isEmpty(contactDetails)){
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private ModelContactDetail mapInContactDetail(final DTOIntContactDetail dtoIntContactDetail) {
        ModelContactDetail model = new ModelContactDetail();
        model.setContact(dtoIntContactDetail.getContact());
        model.setContactType(dtoIntContactDetail.getContactType());
        return model;
    }

    private ModelRelationType mapInRelationType(final DTOIntRelationType relationType) {
        if(relationType == null){
            return null;
        }
        ModelRelationType model = new ModelRelationType();
        model.setId(relationType.getId());
        return  model;
    }

    private ModelProduct mapInProduct(DTOIntRelatedProduct product) {
        if(product == null){
            return null;
        }
        ModelProduct model = new ModelProduct();
        model.setId(product.getId());
        model.setProductType(mapInProductType(product.getProductType()));
        return model;
    }

    private ModelProductType mapInProductType(final DTOIntProductType productType) {
        if(productType == null){
            return null;
        }
        ModelProductType model = new ModelProductType();
        model.setId(productType.getId());
        return model;
    }

    private ModelContract mapInContract(DTOIntContract contract) {
        if(contract == null){
            return null;
        }
        ModelContract model = new ModelContract();
        model.setId(contract.getId());
        return model;
    }

    private ModelNetcashType mapInNetcashType(final DTOIntNetcashType netcashType) {
        if(netcashType == null){
            return null;
        }
        ModelNetcashType model = new ModelNetcashType();
        model.setId(netcashType.getId());
        model.setVersion(mapInVersion(netcashType.getVersion()));
        return model;
    }

    private ModelVersion mapInVersion(final DTOIntVersionProduct version) {
        if(version == null){
            return null;
        }
        ModelVersion model = new ModelVersion();
        model.setId(version.getId());
        return model;
    }

    private ModelBusiness mapInBusiness(final DTOIntBusiness business) {
        if(business == null){
            return null;
        }
        ModelBusiness model = new ModelBusiness();
        model.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        model.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        model.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
        return  model;
    }

    private ModelLimitAmount mapInLimitAmount(final DTOIntLimitAmount limitAmount) {
        if(limitAmount == null){
            return null;
        }
        ModelLimitAmount model = new ModelLimitAmount();
        model.setAmount(limitAmount.getAmount());
        model.setCurrency(limitAmount.getCurrency());
        return model;
    }

    private ModelBusinessManagement mapInBusinessManagement(final DTOIntBusinessManagement businessManagement) {
        if(businessManagement == null){
            return null;
        }
        ModelBusinessManagement model = new ModelBusinessManagement();
        model.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return model;
    }

    private ModelManagementType mapInManagementType(final DTOIntManagementType managementType) {
        if(managementType == null){
            return null;
        }
        ModelManagementType model = new ModelManagementType();
        model.setId(managementType.getId());
        return model;
    }

    private List<ModelBusinessDocument> mapInBusinessDocuments(final List<DTOIntBusinessDocument> businessDocuments) {
        if(CollectionUtils.isEmpty(businessDocuments)){
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapBusinessDocument).collect(Collectors.toList());
    }

    private ModelBusinessDocument mapBusinessDocument(final DTOIntBusinessDocument dtoIntBusinessDocument) {
        ModelBusinessDocument model = new ModelBusinessDocument();
        model.setBusinessDocumentType(mapInBusinessDocumentType(dtoIntBusinessDocument.getBusinessDocumentType()));
        model.setDocumentNumber(dtoIntBusinessDocument.getDocumentNumber());
        model.setIssueDate(dtoIntBusinessDocument.getIssueDate());
        model.setExpirationDate(dtoIntBusinessDocument.getExpirationDate());
        return model;
    }

    private ModelBusinessDocumentType mapInBusinessDocumentType(final DTOIntBusinessDocumentType businessDocumentType) {
        if(businessDocumentType == null){
            return null;
        }
        ModelBusinessDocumentType model = new ModelBusinessDocumentType();
        model.setId(businessDocumentType.getId());
        return model;
    }

    @Override
    public FinancialManagementCompanies mapOut(final ModelFinancialManagementCompanyResponse modelFinancialManagementCompanyResponse) {
        if(modelFinancialManagementCompanyResponse == null){
            return null;
        }
        FinancialManagementCompanies financial = new FinancialManagementCompanies();
        if(modelFinancialManagementCompanyResponse.getData() != null){
            financial.setId(modelFinancialManagementCompanyResponse.getData().getId());
        }
        return financial;
    }
}
