package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestNewUserSimpleMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.IRequestBackendMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Mapper("reactivationUserSimpleMapper")
public class RestReactivationUserSimpleMapper implements IRestNewUserSimpleMapper {

    @Autowired
    private IRequestBackendMapper requestBackendMapper;

    @Override
    public BodyDataRest mapIn(InputRequestBackendRest entityPayload) {
        BodyDataRest bodyDataRest = requestBackendMapper.mapInBodyDataRest(entityPayload.getRequestBody());
        bodyDataRest.getDataOperation().setNewpassword(entityPayload.getRequestBody().getDataOperationNewpassword());
        return bodyDataRest;
    }

    @Override
    public Map<String, String> mapInHeader(InputRequestBackendRest input) {
        return requestBackendMapper.mapInHeaderDataRest(input.getHeader());
    }
}
