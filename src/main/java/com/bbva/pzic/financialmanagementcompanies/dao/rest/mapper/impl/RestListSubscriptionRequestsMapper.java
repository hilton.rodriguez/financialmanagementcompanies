package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntPagination;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessDocumentSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Reviewer;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.StatusSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestListSubscriptionRequestsMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestListSubscriptionRequestsMapper extends ConfigurableMapper implements IRestListSubscriptionRequestsMapper {

    static final String SUBSCRIPTIONS_REQUEST_ID = "subscriptionsRequest.id";
    static final String BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID = "businessDocuments.businessDocumentType.id";
    static final String BUSINESS_DOCUMENTS_DOCUMENT_NUMBER = "businessDocuments.documentNumber";
    static final String STATUS_ID = "status.id";
    static final String UNIT_MANAGEMENT = "unitManagement";
    static final String BRANCH_ID = "branch.id";
    static final String FROM_SUBSCRIPTION_REQUEST_DATE = "fromSubscriptionRequestDate";
    static final String TO_SUBSCRIPTION_REQUEST_DATE = "toSubscriptionRequestDate";
    static final String BUSINESS_ID = "business.id";
    static final String PAGINATION_KEY = "paginationKey";
    static final String PAGE_SIZE = "pageSize";

    private static final Log LOG = LogFactory.getLog(RestListSubscriptionRequestsMapper.class);

    private static final String BUSINESS_ID_MAP = "business.id";
    private static final String STATUS_ID_MAP = "status.id";
    private static final String UNIT_MANAGEMENT_MAP = "unitManagement";
    private static final String BRANCH_ID_MAP = "branch.id";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(SubscriptionRequest.class, ModelSubscriptionRequest.class)
                .field("id", "id")
                .field("business.legalName", "business.legalName")
                .field(BUSINESS_ID_MAP, BUSINESS_ID_MAP)
                .field(STATUS_ID_MAP, STATUS_ID_MAP)
                .field("status.name", "status.name")
                .field("status.reason", "status.reason")
                .field("openingDate", "openingDate")
                .field(UNIT_MANAGEMENT_MAP, UNIT_MANAGEMENT_MAP)
                .field(BRANCH_ID_MAP, BRANCH_ID_MAP)
                .field("branch.name", "branch.name")
                .register();

        factory.classMap(BusinessDocumentSubscriptionRequest.class, ModelBusinessDocumentSubscriptionRequest.class)
                .field("businessDocumentType.id", "businessDocumentType.id")
                .field("businessDocumentType.name", "businessDocumentType.name")
                .field("documentNumber", "documentNumber")
                .register();

        factory.classMap(Reviewer.class, ModelParticipant.class)
                .field("businessAgentId", "code")
                .field("firstName", "firstName")
                .field("middleName", "middleName")
                .field("lastName", "lastName")
                .field("secondLastName", "motherLastName")
                .field("reviewerType.id", "participantType.id")
                .field("reviewerType.name", "participantType.name")
                .register();
    }

    @Override
    public HashMap<String, String> mapIn(InputListSubscriptionRequests input) {
        LOG.info("... called method RestListSubscriptionRequestsMapper.mapIn ...");
        HashMap<String, String> queries = new HashMap<>();
        queries.put(SUBSCRIPTIONS_REQUEST_ID, input.getSubscriptionsRequestId());
        queries.put(BUSINESS_DOCUMENTS_BUSINESS_DOCUMENT_TYPE_ID, input.getBusinessDocumentsBusinessDocumentTypeId());
        queries.put(BUSINESS_DOCUMENTS_DOCUMENT_NUMBER, input.getBusinessDocumentsDocumentNumber());
        queries.put(STATUS_ID, input.getStatusId());
        queries.put(UNIT_MANAGEMENT, input.getUnitManagement());
        queries.put(BRANCH_ID, input.getBranchId());
        queries.put(FROM_SUBSCRIPTION_REQUEST_DATE, input.getFromSubscriptionRequestDate());
        queries.put(TO_SUBSCRIPTION_REQUEST_DATE, input.getToSubscriptionRequestDate());
        queries.put(BUSINESS_ID, input.getBusinessId());
        queries.put(PAGINATION_KEY, input.getPaginationKey());
        if (input.getPageSize() != null) {
            queries.put(PAGE_SIZE, input.getPageSize().toString());
        }
        return queries;
    }

    @Override
    public DTOIntSubscriptionRequests mapOut(ModelSubscriptionRequests modelResponse) {
        LOG.info("... called method RestListSubscriptionRequestsMapper.mapOut ...");
        if (modelResponse.getData() == null) {
            return null;
        }

        DTOIntSubscriptionRequests dtoIntSubscriptionRequests = new DTOIntSubscriptionRequests();
        dtoIntSubscriptionRequests.setData(new ArrayList<SubscriptionRequest>());
        dtoIntSubscriptionRequests.setPagination(mapOutPagination(modelResponse.getPagination()));

        for (ModelSubscriptionRequest modelItem : modelResponse.getData()) {
            SubscriptionRequest item = map(modelItem, SubscriptionRequest.class);
            if (item.getBusiness() != null) {
                item.getBusiness().setBusinessDocuments(mapOutBusinessDocuments(modelItem.getBusiness().getBusinessDocuments()));
            }
            item.setReviewers(mapOutReviewers(modelItem.getParticipants()));
            item.setStatus(mapOutStatus(item.getStatus()));
            dtoIntSubscriptionRequests.getData().add(item);
        }

        return dtoIntSubscriptionRequests;
    }

    private DTOIntPagination mapOutPagination(final ModelPagination pagination) {
        if (pagination == null) {
            return null;
        }
        DTOIntPagination dtoIntPagination = new DTOIntPagination();
        dtoIntPagination.setPageSize(pagination.getPageSize());
        dtoIntPagination.setPaginationKey(pagination.getPaginationKey());
        dtoIntPagination.setPrevious(pagination.getPrevious());
        dtoIntPagination.setPage(pagination.getPage());
        dtoIntPagination.setTotalElements(pagination.getTotalElements());
        dtoIntPagination.setTotalPages(pagination.getTotalPages());

        return dtoIntPagination;
    }

    private List<BusinessDocumentSubscriptionRequest> mapOutBusinessDocuments(List<ModelBusinessDocumentSubscriptionRequest> modelBusinessDocuments) {
        if (CollectionUtils.isEmpty(modelBusinessDocuments)) {
            return null;
        }
        List<BusinessDocumentSubscriptionRequest> businessDocuments = new ArrayList<>();
        for (ModelBusinessDocumentSubscriptionRequest modelBusinessDocument : modelBusinessDocuments) {
            if (modelBusinessDocument.getBusinessDocumentType() != null) {
                modelBusinessDocument.getBusinessDocumentType().setId(enumMapper.getEnumValue("subscriptionRequests.documentType.id", modelBusinessDocument.getBusinessDocumentType().getId()));
            }
            businessDocuments.add(map(modelBusinessDocument, BusinessDocumentSubscriptionRequest.class));
        }
        return businessDocuments;
    }

    private StatusSubscriptionRequest mapOutStatus(StatusSubscriptionRequest status) {
        if (status == null) {
            return null;
        }
        status.setId(enumMapper.getEnumValue("suscriptionRequest.status.id", status.getId()));
        status.setReason(enumMapper.getEnumValue("suscriptionRequest.status.reason.id", status.getReason()));
        return status;
    }

    private List<Reviewer> mapOutReviewers(List<ModelParticipant> modelParticipants) {
        if (CollectionUtils.isEmpty(modelParticipants)) {
            return null;
        }

        List<Reviewer> reviewers = new ArrayList<>();

        for (ModelParticipant modelParticipant : modelParticipants) {
            if (modelParticipant.getParticipantType() != null) {
                modelParticipant.getParticipantType().setId(enumMapper.getEnumValue("suscriptionRequest.reviewer.id", modelParticipant.getParticipantType().getId()));
            }
            reviewers.add(map(modelParticipant, Reviewer.class));
        }

        return reviewers;
    }
}
