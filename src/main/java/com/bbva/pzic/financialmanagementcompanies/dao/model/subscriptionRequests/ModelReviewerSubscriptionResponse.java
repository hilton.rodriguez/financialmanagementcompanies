package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelReviewerSubscriptionResponse {

    private ModelParticipant participant;
    private List<Message> messages;

    public ModelParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(ModelParticipant participant) {
        this.participant = participant;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
