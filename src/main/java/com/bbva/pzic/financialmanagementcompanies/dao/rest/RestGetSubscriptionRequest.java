package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequestData;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestGetSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestGetConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Component
public class RestGetSubscriptionRequest extends RestGetConnection<ModelSubscriptionRequestData> {

    private static final String GET_SUBSCRIPTION_REQUEST_URL = "servicing.url.financialManagementCompanies.getSubscriptionRequest";
    private static final String GET_SUBSCRIPTION_REQUEST_USE_PROXY = "servicing.proxy.financialManagementCompanies.getSubscriptionRequest";

    @Autowired
    private IRestGetSubscriptionRequestMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(GET_SUBSCRIPTION_REQUEST_USE_PROXY, false);
    }

    public SubscriptionRequest invoke(final InputGetSubscriptionRequest input) {
        return mapper.mapOut(connect(GET_SUBSCRIPTION_REQUEST_URL, mapper.mapIn(input)));
    }

    @Override
    protected void evaluateResponse(ModelSubscriptionRequestData response, int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810278", statusCode);
    }
}
