package com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPS0;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public final class FormatsKwfpMock {

    private static final FormatsKwfpMock INSTANCE = new FormatsKwfpMock();
    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private FormatsKwfpMock() {
    }

    public static FormatsKwfpMock getInstance() {
        return INSTANCE;
    }

    public FormatoKNECFPS0 getFormatoKNECFPS0() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/financialmanagementcompanies/dao/model/kwfp/mock/formatoKNECFPS0.json"), FormatoKNECFPS0.class);
    }

    public FormatoKNECFPS0 getFormatoKNECFPS0Empty() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/financialmanagementcompanies/dao/model/kwfp/mock/formatoKNECFPS0-EMPTY.json"), FormatoKNECFPS0.class);
    }
}
