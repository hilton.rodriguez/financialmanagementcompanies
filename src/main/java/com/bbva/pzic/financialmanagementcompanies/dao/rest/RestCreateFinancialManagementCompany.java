package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanies2;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class RestCreateFinancialManagementCompany extends RestPostConnection <ModelFinancialManagementCompanies2, ModelFinancialManagementCompanyResponse> {

    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_URL = "servicing.smc.configuration.SMCPE1810335.backend.url";
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_PROXY = "servicing.smc.configuration.SMCPE1810335.backend.proxy";

    @Autowired
    private IRestCreateFinancialManagementCompanyMapper mapper;

    @PostConstruct
    public void init(){
        useProxy = configurationManager.getBooleanProperty(CREATE_FINANCIAL_MANAGEMENT_COMPANY_PROXY, false);
    }

    public FinancialManagementCompanies invoke(final DTOIntFinancialManagementCompanies input){
        return mapper.mapOut(connect(CREATE_FINANCIAL_MANAGEMENT_COMPANY_URL, mapper.mapIn(input)));
    }

    @Override
    protected void evaluateResponse(ModelFinancialManagementCompanyResponse response, int statusCode) {

    }
}
