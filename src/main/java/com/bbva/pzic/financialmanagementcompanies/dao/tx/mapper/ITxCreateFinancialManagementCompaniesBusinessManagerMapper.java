package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBEHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBSHD;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateFinancialManagementCompaniesBusinessManagerMapper {

    FormatoKNECBEHD mapIn(DTOIntBusinessManager dtoIn);

    String mapOut(FormatoKNECBSHD formatOutput);
}