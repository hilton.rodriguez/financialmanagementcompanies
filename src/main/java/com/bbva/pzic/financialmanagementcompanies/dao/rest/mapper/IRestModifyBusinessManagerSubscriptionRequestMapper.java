package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;

import java.util.Map;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public interface IRestModifyBusinessManagerSubscriptionRequestMapper {

    Map<String, String> mapInPathParams(InputModifyBusinessManagerSubscriptionRequest input);

    ModelSubscriptionRequest mapIn(InputModifyBusinessManagerSubscriptionRequest input);
}