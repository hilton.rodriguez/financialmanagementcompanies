package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNECBSHE</code> de la transacci&oacute;n <code>KWHE</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBSHE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBSHE {

	/**
	 * <p>Campo <code>USUIDS</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "USUIDS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 24, longitudMaxima = 24)
	private String usuids;

	/**
	 * <p>Campo <code>PODVALS</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "PODVALS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String podvals;

	/**
	 * <p>Campo <code>TIPPERS</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "TIPPERS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tippers;

	/**
	 * <p>Campo <code>GERNEG</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "GERNEG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String gerneg;

}