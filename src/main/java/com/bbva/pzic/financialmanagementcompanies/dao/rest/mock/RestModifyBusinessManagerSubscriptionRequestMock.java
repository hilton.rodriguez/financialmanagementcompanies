package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestModifyBusinessManagerSubscriptionRequest;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestModifyBusinessManagerSubscriptionRequestMock extends RestModifyBusinessManagerSubscriptionRequest {

    @Override
    protected Object connect(String urlPropertyValue, Map<String, String> pathParams, ModelSubscriptionRequest entityPayload) {
        return new Object();
    }
}