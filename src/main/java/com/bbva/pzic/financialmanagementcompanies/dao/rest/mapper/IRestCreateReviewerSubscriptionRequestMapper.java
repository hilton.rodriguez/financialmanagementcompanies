package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionResponse;

import java.util.Map;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public interface IRestCreateReviewerSubscriptionRequestMapper {

    Map<String, String> mapInPathParams(InputCreateReviewerSubscriptionRequest input);

    ModelReviewerSubscriptionRequest mapIn(InputCreateReviewerSubscriptionRequest reviewerSubscriptionRequest);

    String mapOut(ModelReviewerSubscriptionResponse response);
}