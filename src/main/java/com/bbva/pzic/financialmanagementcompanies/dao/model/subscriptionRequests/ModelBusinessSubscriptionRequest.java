package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelBusinessSubscriptionRequest {

    @DatoAuditable(omitir = true)
    private String id;
    private List<ModelBusinessDocumentSubscriptionRequest> businessDocuments;
    private ModelBusinessManagement businessManagement;
    @DatoAuditable(omitir = true)
    private String legalName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ModelBusinessDocumentSubscriptionRequest> getBusinessDocuments() {
        return businessDocuments;
    }

    public void setBusinessDocuments(
            List<ModelBusinessDocumentSubscriptionRequest> businessDocuments) {
        this.businessDocuments = businessDocuments;
    }

    public ModelBusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public void setBusinessManagement(ModelBusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }
}