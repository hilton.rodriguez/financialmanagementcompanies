package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelBusinessManagement {

    private ModelManagementType managementType;

    public ModelManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ModelManagementType managementType) {
        this.managementType = managementType;
    }
}