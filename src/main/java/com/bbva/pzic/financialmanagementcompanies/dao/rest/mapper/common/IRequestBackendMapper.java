package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestHeader;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;

import java.util.Map;

public interface IRequestBackendMapper {

    BodyDataRest mapInBodyDataRest(DTOIntRequestBody requestBody);

    Map<String, String> mapInHeaderDataRest(DTOIntRequestHeader requestHeader);
}
