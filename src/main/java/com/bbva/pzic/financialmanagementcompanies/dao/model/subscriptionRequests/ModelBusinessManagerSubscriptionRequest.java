package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelBusinessManagerSubscriptionRequest {

    @DatoAuditable(omitir = true)
    private String firstName;
    @DatoAuditable(omitir = true)
    private String middleName;
    @DatoAuditable(omitir = true)
    private String lastName;
    @DatoAuditable(omitir = true)
    private String secondLastName;
    private List<ModelIdentityDocumentSubscriptionRequest> identityDocuments;
    private List<ModelContactDetail> contactDetails;
    private List<ModelRole> roles;
    private String id;
    private String targetUserId;
    private String userId;
    @DatoAuditable(omitir = true)
    private String userIdHost;
    private String motherLastName;
    private List<ModelContactInformation> contactInformations;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public List<ModelIdentityDocumentSubscriptionRequest> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(List<ModelIdentityDocumentSubscriptionRequest> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public List<ModelContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ModelContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }

    public String getMotherLastName() {
        return motherLastName;
    }

    public void setMotherLastName(String motherLastName) {
        this.motherLastName = motherLastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIdHost() {
        return userIdHost;
    }

    public void setUserIdHost(String userIdHost) {
        this.userIdHost = userIdHost;
    }

    public List<ModelContactInformation> getContactInformations() {
        return contactInformations;
    }

    public void setContactInformations(List<ModelContactInformation> contactInformations) {
        this.contactInformations = contactInformations;
    }

    public List<ModelRole> getRoles() {
        return roles;
    }

    public void setRoles(List<ModelRole> roles) {
        this.roles = roles;
    }
}