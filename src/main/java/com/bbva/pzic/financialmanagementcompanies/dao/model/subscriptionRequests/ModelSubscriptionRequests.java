package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelSubscriptionRequests {

    private List<ModelSubscriptionRequest> data;
    private ModelPagination pagination;
    private List<Message> messages;

    public List<ModelSubscriptionRequest> getData() {
        return data;
    }

    public void setData(List<ModelSubscriptionRequest> data) {
        this.data = data;
    }

    public ModelPagination getPagination() {
        return pagination;
    }

    public void setPagination(ModelPagination pagination) {
        this.pagination = pagination;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}