package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionKwlv")
public class TransaccionKwlv implements InvocadorTransaccion<PeticionTransaccionKwlv, RespuestaTransaccionKwlv> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKwlv invocar(PeticionTransaccionKwlv transaccion) {
        return servicioTransacciones.invocar(PeticionTransaccionKwlv.class, RespuestaTransaccionKwlv.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKwlv invocarCache(PeticionTransaccionKwlv transaccion) {
        return servicioTransacciones.invocar(PeticionTransaccionKwlv.class, RespuestaTransaccionKwlv.class, transaccion);
    }

    @Override
    public void vaciarCache() {
    }
}
