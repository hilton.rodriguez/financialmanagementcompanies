package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBSHD;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public final class FormatsKwhdMock {

    private static final FormatsKwhdMock INSTANCE = new FormatsKwhdMock();

    public static FormatsKwhdMock getInstance() {
        return INSTANCE;
    }

    public FormatoKNECBSHD getFormatoKNECBSHD() {
        FormatoKNECBSHD formato = new FormatoKNECBSHD();
        formato.setUsuid("H71thbpwa9gtVqr17OF4Vfra");
        return formato;
    }
}