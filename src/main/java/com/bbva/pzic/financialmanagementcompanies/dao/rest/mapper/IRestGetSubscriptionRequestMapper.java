package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequestData;

import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IRestGetSubscriptionRequestMapper {

    Map<String, String> mapIn(InputGetSubscriptionRequest input);

    SubscriptionRequest mapOut(ModelSubscriptionRequestData response);
}
