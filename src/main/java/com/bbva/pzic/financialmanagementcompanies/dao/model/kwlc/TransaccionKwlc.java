package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionKwlc")
public class TransaccionKwlc implements InvocadorTransaccion<PeticionTransaccionKwlc, RespuestaTransaccionKwlc> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKwlc invocar(PeticionTransaccionKwlc transaccion) {
        return servicioTransacciones.invocar(PeticionTransaccionKwlc.class, RespuestaTransaccionKwlc.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKwlc invocarCache(PeticionTransaccionKwlc transaccion) {
        return servicioTransacciones.invocar(PeticionTransaccionKwlc.class, RespuestaTransaccionKwlc.class, transaccion);
    }

    @Override
    public void vaciarCache() {
    }
}
