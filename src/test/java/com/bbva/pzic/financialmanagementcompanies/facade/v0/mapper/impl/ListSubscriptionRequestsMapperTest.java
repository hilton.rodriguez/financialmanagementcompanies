package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class ListSubscriptionRequestsMapperTest {

    @InjectMocks
    private ListSubscriptionRequestsMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapBackendValues();
    }

    private void mapBackendValues() {
        Mockito.when(enumMapper.getBackendValue("subscriptionRequests.documentType.id", DNI_DOCUMENT_TYPE_ID_ENUM)).thenReturn(DNI_DOCUMENT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("suscriptionRequest.status.id", APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM)).thenReturn(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND);
    }

    @Test
    public void mapInFullTest() {
        InputListSubscriptionRequests result = mapper.mapIn(EntityStubs.SUBSCRIPTIONS_REQUEST_ID, EntityStubs.DNI_DOCUMENT_TYPE_ID_ENUM, EntityStubs.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER, EntityStubs.APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM, EntityStubs.UNIT_MANAGEMENT, EntityStubs.BRANCH_ID, EntityStubs.FROM_SUBSCRIPTION_REQUEST_DATE, EntityStubs.TO_SUBSCRIPTION_REQUEST_DATE, EntityStubs.BUSINESS_ID, EntityStubs.PAGINATION_KEY, EntityStubs.PAGE_SIZE);
        assertNotNull(result);
        assertNotNull(result.getSubscriptionsRequestId());
        assertNotNull(result.getBusinessDocumentsBusinessDocumentTypeId());
        assertNotNull(result.getBusinessDocumentsDocumentNumber());
        assertNotNull(result.getStatusId());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranchId());
        assertNotNull(result.getFromSubscriptionRequestDate());
        assertNotNull(result.getToSubscriptionRequestDate());
        assertNotNull(result.getBusinessId());
        assertNotNull(result.getPaginationKey());
        assertNotNull(result.getPageSize());
        assertEquals(EntityStubs.SUBSCRIPTIONS_REQUEST_ID, result.getSubscriptionsRequestId());
        assertEquals(EntityStubs.DNI_DOCUMENT_TYPE_ID_BACKEND, result.getBusinessDocumentsBusinessDocumentTypeId());
        assertEquals(EntityStubs.BUSINESS_DOCUMENTS_DOCUMENT_NUMBER, result.getBusinessDocumentsDocumentNumber());
        assertEquals(EntityStubs.APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND, result.getStatusId());
        assertEquals(EntityStubs.UNIT_MANAGEMENT, result.getUnitManagement());
        assertEquals(EntityStubs.BRANCH_ID, result.getBranchId());
        assertEquals(EntityStubs.FROM_SUBSCRIPTION_REQUEST_DATE, result.getFromSubscriptionRequestDate());
        assertEquals(EntityStubs.TO_SUBSCRIPTION_REQUEST_DATE, result.getToSubscriptionRequestDate());
        assertEquals(EntityStubs.BUSINESS_ID, result.getBusinessId());
        assertEquals(EntityStubs.PAGINATION_KEY, result.getPaginationKey());
        assertEquals(EntityStubs.PAGE_SIZE, result.getPageSize());
    }

    @Test
    public void mapInEmptyTest() {
        InputListSubscriptionRequests result = mapper.mapIn(null, null, null,
                null, null, null, null, null, null, null, null);
        assertNotNull(result);
        assertNull(result.getSubscriptionsRequestId());
        assertNull(result.getBusinessDocumentsBusinessDocumentTypeId());
        assertNull(result.getBusinessDocumentsDocumentNumber());
        assertNull(result.getStatusId());
        assertNull(result.getUnitManagement());
        assertNull(result.getBranchId());
        assertNull(result.getFromSubscriptionRequestDate());
        assertNull(result.getToSubscriptionRequestDate());
        assertNull(result.getBusinessId());
        assertNull(result.getPaginationKey());
        assertNull(result.getPageSize());
    }

    @Test
    public void mapOutFullTest() {
        SubscriptionRequests result = mapper.mapOut(EntityStubs.getInstance().getDTOIntSubscriptionRequests());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        SubscriptionRequests result = mapper.mapOut(null);
        assertNull(result);
    }
}
