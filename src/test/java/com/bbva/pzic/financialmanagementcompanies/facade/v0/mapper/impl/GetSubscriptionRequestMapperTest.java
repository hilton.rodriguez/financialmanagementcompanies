package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IGetSubscriptionRequestMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class GetSubscriptionRequestMapperTest {

    private IGetSubscriptionRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new GetSubscriptionRequestMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetSubscriptionRequest result = mapper.mapIn(EntityStubs.SUBSCRIPTION_REQUEST_ID);
        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertEquals(EntityStubs.SUBSCRIPTION_REQUEST_ID, result.getSubscriptionRequestId());
    }
}
