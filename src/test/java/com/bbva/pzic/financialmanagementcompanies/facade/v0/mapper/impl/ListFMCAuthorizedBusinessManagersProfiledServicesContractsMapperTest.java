package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ServiceContract;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class ListFMCAuthorizedBusinessManagersProfiledServicesContractsMapperTest {

    private IListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper mapper;

    @Before
    public void setUp() {
        mapper = new ListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper();
    }

    @Test
    public void mapInFullTest() {
        InputListFMCAuthorizedBusinessManagersProfiledServicesContracts result = mapper.mapIn(
                EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID,
                EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID,
                EntityStubs.PROFILED_SERVICE_ID,
                EntityStubs.PAGINATION_KEY,
                EntityStubs.PAGE_SIZE);

        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getAuthorizedBusinessManagerId());
        assertNotNull(result.getProfiledServiceId());
        assertNotNull(result.getPaginationKey());
        assertNotNull(result.getPageSize());

        assertEquals(EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, result.getFinancialManagementCompanyId());
        assertEquals(EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID, result.getAuthorizedBusinessManagerId());
        assertEquals(EntityStubs.PROFILED_SERVICE_ID, result.getProfiledServiceId());
        assertEquals(EntityStubs.PAGINATION_KEY, result.getPaginationKey());
        assertEquals(EntityStubs.PAGE_SIZE, result.getPageSize());
    }

    @Test
    public void mapInEmptyTest() {
        InputListFMCAuthorizedBusinessManagersProfiledServicesContracts result = mapper.mapIn(
                EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID,
                EntityStubs.PROFILED_SERVICE_ID, null, null);

        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getAuthorizedBusinessManagerId());
        assertNotNull(result.getProfiledServiceId());
        assertNull(result.getPaginationKey());
        assertNull(result.getPageSize());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<ServiceContract>> result = mapper.mapOut(Collections.singletonList(new ServiceContract()), new Pagination());
        assertNotNull(result.getData());
        assertNotNull(result.getPagination());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<ServiceContract>> result = mapper.mapOut(Collections.emptyList(), null);
        assertNull(result);
    }
}
