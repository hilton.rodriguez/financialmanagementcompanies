package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanies2;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateReviewerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.Assert.*;

public class RestCreateFinancialManagementCompanyMapperTest {

    private IRestCreateFinancialManagementCompanyMapper mapper;

    @Before
    public void setUp(){
        mapper = new RestCreateFinancialManagementCompanyMapper();
    }

    @Test
    public void mapInFullTest() throws IOException{
        DTOIntFinancialManagementCompanies input = EntityStubs.getInstance().getDtoIntFinancialManagementCompanies();

        ModelFinancialManagementCompanies2 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getBusiness());
        assertNotNull(result.getBusiness().getBusinessDocuments());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessManagement());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getContract());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getProductType());
        assertNotNull(result.getProduct().getProductType().getId());
        assertNotNull(result.getRelationType());
        assertNotNull(result.getRelationType().getId());
        assertNotNull(result.getReviewers());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getContactDetails());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getReviewerType());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());
    }

    @Test
    public void mapInEmptyTest(){
        ModelFinancialManagementCompanies2 result = mapper.mapIn(new DTOIntFinancialManagementCompanies());

        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getNetcashType());
        assertNull(result.getContract());
        assertNull(result.getProduct());
        assertNull(result.getRelationType());
        assertNull(result.getReviewers());
    }

    @Test
    public void mapOutFullTest(){
        ModelFinancialManagementCompanyResponse response = ResponseFinancialManagementCompaniesMock.getInstance().buildModelFinancialManagementCompanyResponse();
        FinancialManagementCompanies result = mapper.mapOut(response);

        assertNotNull(result);
        assertNotNull(response.getData().getId());
    }

    @Test
    public void mapOutEmptyTest(){
        ModelFinancialManagementCompanyResponse response = ResponseFinancialManagementCompaniesMock.getInstance().buildModelFinancialManagementCompanyResponse();
        response.getData().setId(null);

        FinancialManagementCompanies result = mapper.mapOut(new ModelFinancialManagementCompanyResponse());

        assertNotNull(result);

        assertNull(result.getId());
    }

}