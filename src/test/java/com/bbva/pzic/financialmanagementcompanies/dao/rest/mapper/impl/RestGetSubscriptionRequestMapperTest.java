package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequestData;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Map;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl.RestGetSubscriptionRequestMapper.PATH_PARAM_SUBSCRIPTION_REQUEST_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestGetSubscriptionRequestMapperTest {

    private ResponseFinancialManagementCompaniesMock mock = ResponseFinancialManagementCompaniesMock.getInstance();

    @InjectMocks
    private RestGetSubscriptionRequestMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Before
    public void init() {
        mapOutEnum();
    }

    private void mapOutEnum() {
        when(enumMapper.getEnumValue("subscriptionRequests.documentType.id", RUC_DOCUMENT_TYPE_ID_BACKEND)).thenReturn(RUC_DOCUMENT_TYPE_ID_ENUM);
        when(enumMapper.getEnumValue("suscriptionRequest.product.id", PRODUCT_ID_BACKEND)).thenReturn(PRODUCT_ID_ENUM);
        when(enumMapper.getEnumValue("campaigns.offers.productType", ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_BACKEND)).thenReturn(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM);
        when(enumMapper.getEnumValue("loans.relatedContracts.relationType", PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_BACKEND)).thenReturn(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM);
        when(enumMapper.getEnumValue("suscriptionRequest.status.id", APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND)).thenReturn(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM);
        when(enumMapper.getEnumValue("suscriptionRequest.status.reason.id", INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_BACKEND)).thenReturn(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM);
        when(enumMapper.getEnumValue("joint.id", JOINT_SIGNATURE_OF_TWO_JOINT_ID_BACKEND)).thenReturn(JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM);
    }

    private void mapOutExpandReviewersEnum() {
        when(enumMapper.getEnumValue("contactDetails.contactType.id", EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND)).thenReturn(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM);
        when(enumMapper.getEnumValue("suscriptionRequest.reviewer.id", EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_BACKEND)).thenReturn(EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_ENUM);
    }

    private void mapOutExpandBusinessManagersEnum() {
        when(enumMapper.getEnumValue("subscriptionRequests.documentType.id", DNI_DOCUMENT_TYPE_ID_BACKEND)).thenReturn(DNI_DOCUMENT_TYPE_ID_ENUM);
        when(enumMapper.getEnumValue("businessManagers.role.id", LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_BACKEND)).thenReturn(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_ENUM);
    }

    @Test
    public void mapInFullTest() {
        InputGetSubscriptionRequest input = EntityStubs.getInstance().getInputGetSubscriptionRequest();
        Map<String, String> result = mapper.mapIn(input);
        assertNotNull(result);

        assertNotNull(result.get(PATH_PARAM_SUBSCRIPTION_REQUEST_ID));
        assertEquals(input.getSubscriptionRequestId(), result.get(PATH_PARAM_SUBSCRIPTION_REQUEST_ID));
    }

    @Test
    public void mapInEmptyTest() {
        Map<String, String> result = mapper.mapIn(new InputGetSubscriptionRequest());
        assertNotNull(result);

        assertNull(result.get(PATH_PARAM_SUBSCRIPTION_REQUEST_ID));
    }

    @Test
    public void mapOutFullTest() throws IOException {
        mapOutExpandReviewersEnum();
        mapOutExpandBusinessManagersEnum();

        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();

        when(enumMapper.getEnumValue("financialManagementCompany.businessManagement.managementType.id",
                response.getData().getBusiness().getBusinessManagement().getManagementType().getId())).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_FRONTEND);
        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);

        assertNotNull(result.getId());
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getDescription());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getName());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getReason());
        assertNotNull(result.getStatus().getUpdateDate());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getJoint().getName());
        assertNotNull(result.getOpeningDate());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());
        assertNotNull(result.getBranch().getName());

        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getFirstName());
        assertNotNull(result.getReviewers().get(0).getMiddleName());
        assertNotNull(result.getReviewers().get(0).getLastName());
        assertNotNull(result.getReviewers().get(0).getSecondLastName());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getName());

        assertNotNull(result.getBusinessManagers().get(0).getId());
        assertNotNull(result.getBusinessManagers().get(0).getTargetUserId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0).getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0).getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles().get(0).getId());

        assertEquals(response.getData().getId(), result.getId());
        assertEquals(response.getData().getBusiness().getId(), result.getBusiness().getId());
        assertEquals(response.getData().getBusiness().getLegalName(), result.getBusiness().getLegalName());
        assertEquals(RUC_DOCUMENT_TYPE_ID_ENUM, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName(), result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_FRONTEND, result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(response.getData().getBusiness().getBusinessManagement().getManagementType().getDescription(), result.getBusiness().getBusinessManagement().getManagementType().getDescription());
        assertEquals(PRODUCT_ID_ENUM, result.getProduct().getId());
        assertEquals(response.getData().getProduct().getName(), result.getProduct().getName());
        assertEquals(response.getData().getLimitAmount().getAmount(), result.getLimitAmount().getAmount());
        assertEquals(response.getData().getLimitAmount().getCurrency(), result.getLimitAmount().getCurrency());
        assertEquals(response.getData().getRelatedContracts().get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getName(), result.getRelatedContracts().get(0).getProduct().getName());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getProductType().getName(), result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM, result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getRelationType().getName(), result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM, result.getStatus().getId());
        assertEquals(response.getData().getStatus().getName(), result.getStatus().getName());
        assertEquals(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM, result.getStatus().getReason());
        assertEquals(response.getData().getStatus().getUpdateDate(), result.getStatus().getUpdateDate());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM, result.getJoint().getId());
        assertEquals(response.getData().getJoint().getName(), result.getJoint().getName());
        assertEquals(response.getData().getUnitManagement(), result.getUnitManagement());
        assertEquals(response.getData().getBranch().getId(), result.getBranch().getId());
        assertEquals(response.getData().getBranch().getName(), result.getBranch().getName());

        assertEquals(response.getData().getParticipants().get(0).getCode(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(response.getData().getParticipants().get(0).getFirstName(), result.getReviewers().get(0).getFirstName());
        assertEquals(response.getData().getParticipants().get(0).getMiddleName(), result.getReviewers().get(0).getMiddleName());
        assertEquals(response.getData().getParticipants().get(0).getLastName(), result.getReviewers().get(0).getLastName());
        assertEquals(response.getData().getParticipants().get(0).getMotherLastName(), result.getReviewers().get(0).getSecondLastName());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM, result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertEquals(response.getData().getParticipants().get(0).getContactInformations().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals(EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_ENUM, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(response.getData().getParticipants().get(0).getParticipantType().getName(), result.getReviewers().get(0).getReviewerType().getName());

        assertNotNull(response.getData().getBusinessManagers().get(0).getUserId(), result.getBusinessManagers().get(0).getId());
        assertNotNull(response.getData().getBusinessManagers().get(0).getUserIdHost(), result.getBusinessManagers().get(0).getTargetUserId());
        assertNotNull(response.getData().getBusinessManagers().get(0).getFirstName(), result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getMiddleName(), result.getBusinessManagers().get(0).getMiddleName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getLastName(), result.getBusinessManagers().get(0).getLastName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getMotherLastName(), result.getBusinessManagers().get(0).getSecondLastName());
        assertNotNull(DNI_DOCUMENT_TYPE_ID_ENUM, result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM, result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(response.getData().getBusinessManagers().get(0).getContactInformations().get(0).getContact(), result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_ENUM, result.getBusinessManagers().get(0).getRoles().get(0).getId());

        assertEquals(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertEquals(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName());
        assertEquals(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());

        assertEquals(response.getData().getBusinessManagers().get(0).getRoles().get(0).getId(), result.getBusinessManagers().get(0).getRoles().get(0).getId());
    }

    @Test
    public void mapOutFullWithoutReviewersAndManagementTypeDescriptionTest() throws IOException {
        when(enumMapper.getEnumValue("contactDetails.contactType.id", EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND)).thenReturn(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM);
        mapOutExpandBusinessManagersEnum();

        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setParticipants(null);
        response.getData().getBusiness().getBusinessManagement().getManagementType().setDescription(null);

        when(enumMapper.getEnumValue("financialManagementCompany.businessManagement.managementType.id",
                response.getData().getBusiness().getBusinessManagement().getManagementType().getId())).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_FRONTEND);
        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);

        assertNotNull(result.getId());
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNull(result.getBusiness().getBusinessManagement().getManagementType().getDescription());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getName());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getReason());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getJoint().getName());
        assertNotNull(result.getOpeningDate());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());
        assertNotNull(result.getBranch().getName());

        assertNull(result.getReviewers());

        assertNotNull(result.getBusinessManagers().get(0).getId());
        assertNotNull(result.getBusinessManagers().get(0).getTargetUserId());
        assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(result.getBusinessManagers().get(0).getMiddleName());
        assertNotNull(result.getBusinessManagers().get(0).getLastName());
        assertNotNull(result.getBusinessManagers().get(0).getSecondLastName());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName());
        assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_FRONTEND, result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getBusinessManagers().get(0).getRoles().get(0).getId());

        assertEquals(response.getData().getId(), result.getId());
        assertEquals(response.getData().getBusiness().getId(), result.getBusiness().getId());
        assertEquals(response.getData().getBusiness().getLegalName(), result.getBusiness().getLegalName());
        assertEquals(RUC_DOCUMENT_TYPE_ID_ENUM, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName(), result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(PRODUCT_ID_ENUM, result.getProduct().getId());
        assertEquals(response.getData().getProduct().getName(), result.getProduct().getName());
        assertEquals(response.getData().getLimitAmount().getAmount(), result.getLimitAmount().getAmount());
        assertEquals(response.getData().getLimitAmount().getCurrency(), result.getLimitAmount().getCurrency());
        assertEquals(response.getData().getRelatedContracts().get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getName(), result.getRelatedContracts().get(0).getProduct().getName());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getProductType().getName(), result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM, result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getRelationType().getName(), result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM, result.getStatus().getId());
        assertEquals(response.getData().getStatus().getName(), result.getStatus().getName());
        assertEquals(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM, result.getStatus().getReason());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM, result.getJoint().getId());
        assertEquals(response.getData().getJoint().getName(), result.getJoint().getName());
        assertEquals(response.getData().getUnitManagement(), result.getUnitManagement());
        assertEquals(response.getData().getBranch().getId(), result.getBranch().getId());
        assertEquals(response.getData().getBranch().getName(), result.getBranch().getName());

        assertNotNull(response.getData().getBusinessManagers().get(0).getUserId(), result.getBusinessManagers().get(0).getId());
        assertNotNull(response.getData().getBusinessManagers().get(0).getUserIdHost(), result.getBusinessManagers().get(0).getTargetUserId());
        assertNotNull(response.getData().getBusinessManagers().get(0).getFirstName(), result.getBusinessManagers().get(0).getFirstName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getMiddleName(), result.getBusinessManagers().get(0).getMiddleName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getLastName(), result.getBusinessManagers().get(0).getLastName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getMotherLastName(), result.getBusinessManagers().get(0).getSecondLastName());
        assertNotNull(DNI_DOCUMENT_TYPE_ID_ENUM, result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        assertNotNull(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getName());
        assertNotNull(response.getData().getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        assertNotNull(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM, result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(response.getData().getBusinessManagers().get(0).getContactInformations().get(0).getContact(), result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(LEGAL_REPRESENTATIVE_BUSINESS_MANAGERS_ROLE_ID_ENUM, result.getBusinessManagers().get(0).getRoles().get(0).getId());
    }

    @Test
    public void mapOutFullWithoutBusinessManagersTest() throws IOException {
        mapOutExpandReviewersEnum();

        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setBusinessManagers(null);
        response.getData().getBusiness().getBusinessManagement().getManagementType().setId(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);

        assertNotNull(result.getId());
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getDescription());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getName());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getReason());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getJoint().getName());
        assertNotNull(result.getOpeningDate());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());
        assertNotNull(result.getBranch().getName());

        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getFirstName());
        assertNotNull(result.getReviewers().get(0).getMiddleName());
        assertNotNull(result.getReviewers().get(0).getLastName());
        assertNotNull(result.getReviewers().get(0).getSecondLastName());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getName());
        assertNull(result.getBusinessManagers());

        assertEquals(response.getData().getId(), result.getId());
        assertEquals(response.getData().getBusiness().getId(), result.getBusiness().getId());
        assertEquals(response.getData().getBusiness().getLegalName(), result.getBusiness().getLegalName());
        assertEquals(RUC_DOCUMENT_TYPE_ID_ENUM, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName(), result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(response.getData().getBusiness().getBusinessManagement().getManagementType().getDescription(), result.getBusiness().getBusinessManagement().getManagementType().getDescription());
        assertEquals(PRODUCT_ID_ENUM, result.getProduct().getId());
        assertEquals(response.getData().getProduct().getName(), result.getProduct().getName());
        assertEquals(response.getData().getLimitAmount().getAmount(), result.getLimitAmount().getAmount());
        assertEquals(response.getData().getLimitAmount().getCurrency(), result.getLimitAmount().getCurrency());
        assertEquals(response.getData().getRelatedContracts().get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getName(), result.getRelatedContracts().get(0).getProduct().getName());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getProductType().getName(), result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM, result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getRelationType().getName(), result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM, result.getStatus().getId());
        assertEquals(response.getData().getStatus().getName(), result.getStatus().getName());
        assertEquals(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM, result.getStatus().getReason());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM, result.getJoint().getId());
        assertEquals(response.getData().getJoint().getName(), result.getJoint().getName());
        assertEquals(response.getData().getUnitManagement(), result.getUnitManagement());
        assertEquals(response.getData().getBranch().getId(), result.getBranch().getId());
        assertEquals(response.getData().getBranch().getName(), result.getBranch().getName());

        assertEquals(response.getData().getParticipants().get(0).getCode(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(response.getData().getParticipants().get(0).getFirstName(), result.getReviewers().get(0).getFirstName());
        assertEquals(response.getData().getParticipants().get(0).getMiddleName(), result.getReviewers().get(0).getMiddleName());
        assertEquals(response.getData().getParticipants().get(0).getLastName(), result.getReviewers().get(0).getLastName());
        assertEquals(response.getData().getParticipants().get(0).getMotherLastName(), result.getReviewers().get(0).getSecondLastName());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM, result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertEquals(response.getData().getParticipants().get(0).getContactInformations().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals(EXECUTIVE_SUSCRIPTION_REQUEST_REVIEWER_ENUM, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(response.getData().getParticipants().get(0).getParticipantType().getName(), result.getReviewers().get(0).getReviewerType().getName());
    }

    @Test
    public void mapOutFullWithoutExpandsTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setParticipants(null);
        response.getData().setBusinessManagers(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);

        assertNotNull(result.getId());
        assertNotNull(result.getBusiness().getId());
        assertNotNull(result.getBusiness().getLegalName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getLimitAmount().getAmount());
        assertNotNull(result.getLimitAmount().getCurrency());
        assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getName());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getReason());
        assertNotNull(result.getJoint().getId());
        assertNotNull(result.getJoint().getName());
        assertNotNull(result.getOpeningDate());
        assertNotNull(result.getUnitManagement());
        assertNotNull(result.getBranch().getId());
        assertNotNull(result.getBranch().getName());
        assertNull(result.getReviewers());
        assertNull(result.getBusinessManagers());

        assertEquals(response.getData().getId(), result.getId());
        assertEquals(response.getData().getBusiness().getId(), result.getBusiness().getId());
        assertEquals(response.getData().getBusiness().getLegalName(), result.getBusiness().getLegalName());
        assertEquals(RUC_DOCUMENT_TYPE_ID_ENUM, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName(), result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getName());
        assertEquals(response.getData().getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(PRODUCT_ID_ENUM, result.getProduct().getId());
        assertEquals(response.getData().getProduct().getName(), result.getProduct().getName());
        assertEquals(response.getData().getLimitAmount().getAmount(), result.getLimitAmount().getAmount());
        assertEquals(response.getData().getLimitAmount().getCurrency(), result.getLimitAmount().getCurrency());
        assertEquals(response.getData().getRelatedContracts().get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getName(), result.getRelatedContracts().get(0).getProduct().getName());
        assertEquals(ACCOUNTS_CAMPAIGNS_OFFERS_PRODUCT_TYPE_ENUM, result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getProduct().getProductType().getName(), result.getRelatedContracts().get(0).getProduct().getProductType().getName());
        assertEquals(PAYING_ACCOUNT_LOANS_RELATED_CONTRACTS_RELATION_TYPE_ENUM, result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(response.getData().getRelatedContracts().get(0).getRelationType().getName(), result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM, result.getStatus().getId());
        assertEquals(response.getData().getStatus().getName(), result.getStatus().getName());
        assertEquals(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM, result.getStatus().getReason());
        assertEquals(JOINT_SIGNATURE_OF_TWO_JOINT_ID_ENUM, result.getJoint().getId());
        assertEquals(response.getData().getJoint().getName(), result.getJoint().getName());
        assertEquals(response.getData().getUnitManagement(), result.getUnitManagement());
        assertEquals(response.getData().getBranch().getId(), result.getBranch().getId());
        assertEquals(response.getData().getBranch().getName(), result.getBranch().getName());
    }

    @Test
    public void mapOutFullWithoutBusinessDocumentsAndManagementTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().getBusiness().setBusinessDocuments(null);
        response.getData().getBusiness().getBusinessManagement().getManagementType().setId(null);
        response.getData().getBusiness().getBusinessManagement().getManagementType().setDescription(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNotNull(result.getBusiness());
        assertNull(result.getBusiness().getBusinessDocuments());
        assertNull(result.getBusiness().getBusinessManagement());
    }

    @Test
    public void mapOutFullWithoutProductAndBusinessManagementTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setProduct(null);
        response.getData().getBusiness().getBusinessManagement().setManagementType(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNull(result.getProduct());
        assertNull(result.getBusiness().getBusinessManagement());
    }

    @Test
    public void mapOutFullWithoutRelatedContractsTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setRelatedContracts(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNull(result.getRelatedContracts());
    }

    @Test
    public void mapOutFullWithoutStatusTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setStatus(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNull(result.getStatus());
    }

    @Test
    public void mapOutFullWithoutJointTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().setJoint(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNull(result.getJoint());
    }

    @Test
    public void mapOutFullWithoutContactDetailsTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().getParticipants().get(0).setContactInformations(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNotNull(result.getReviewers());
        assertFalse(result.getReviewers().isEmpty());
        assertNull(result.getReviewers().get(0).getContactDetails());
    }

    @Test
    public void mapOutFullWithoutIdentityDocumentsTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().getBusinessManagers().get(0).setIdentityDocuments(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNotNull(result.getBusinessManagers());
        assertFalse(result.getBusinessManagers().isEmpty());
        assertNull(result.getBusinessManagers().get(0).getIdentityDocuments());
    }

    @Test
    public void mapOutFullWithoutRolesTest() throws IOException {
        ModelSubscriptionRequestData response = mock.buildResponseModelSubscriptionRequestData();
        response.getData().getBusinessManagers().get(0).setRoles(null);

        SubscriptionRequest result = mapper.mapOut(response);
        assertNotNull(result);
        assertNotNull(result.getBusinessManagers());
        assertFalse(result.getBusinessManagers().isEmpty());
        assertNull(result.getBusinessManagers().get(0).getRoles());
    }

    @Test
    public void mapOutEmptyTest() {
        SubscriptionRequest result = mapper.mapOut(new ModelSubscriptionRequestData());
        assertNull(result);
    }

    @Test
    public void mapOutNullTest() {
        SubscriptionRequest result = mapper.mapOut(null);
        assertNull(result);
    }
}
